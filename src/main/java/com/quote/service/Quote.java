package com.quote.service;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class Quote {
    private Date timestamp;

    private String symbol;

    private String sharesTraded;

    private float priceTraded;

    private ChangeDirection direction;

    private float changeAmount;
}
