package com.quote.service;

import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;

@Service
public class QuoteService {
    private List<Quote> listQuotes;

    private List<String> frequentTickers;

    private final int maxTimeToLiveSeconds = 600;
    private final int SCHEDULE_CHECK_TIME = 1; //in seconds
    private final int NUM_FREQUENT_QUOTES = 5;

    Timer timer = new Timer("timer");

    @PostConstruct
    private void init() {
        listQuotes = Collections.synchronizedList(new LinkedList<>());
        frequentTickers = Collections.synchronizedList(new ArrayList<>());

        timer.scheduleAtFixedRate(timerTask, 0, SCHEDULE_CHECK_TIME * 1000);
    }

    public void putQuote(Quote quote) {
        listQuotes.add(quote);
        processQuote();
    }

    public List<String> getFrequentTickers() {
        return frequentTickers;
    }

    private TimerTask timerTask = new TimerTask() {
        @Override
        public void run() {
            clearExpiredElements();
        }
    };

    /*
       This function removes elements from cache that are expired
    */
    private void clearExpiredElements() {
        long currentTime = new Date().getTime();

        Iterator<Quote> iter = listQuotes.iterator();
        synchronized (listQuotes) {
            while (iter.hasNext()) {
                Quote quote = iter.next();
                if (currentTime - quote.getTimestamp().getTime() >= maxTimeToLiveSeconds)
                    iter.remove();
            }
        }

        processQuote();
    }

    public void processQuote() {
        Map<String, Integer> mapQuotes = new HashMap<>();

        Iterator<Quote> iter = listQuotes.iterator();
        synchronized (listQuotes) {
            while (iter.hasNext()) {
                Quote quote = iter.next();
                mapQuotes.put(quote.getSymbol(),mapQuotes.getOrDefault(quote.getSymbol(),0) + 1);
            }
        }

        QuoteFrequency[] array = new QuoteFrequency[mapQuotes.size()];

        int i =0;
        for (Map.Entry<String, Integer> entry : mapQuotes.entrySet()) {
            array[i].quote = entry.getKey();
            array[i].frequency = entry.getValue();
            i++;
        }

        quickSelect(array, 0, array.length - 1, 5);

        frequentTickers.clear();
        for (i = 0; i < NUM_FREQUENT_QUOTES; i++) {
            frequentTickers.add(array[i].quote);
            System.out.println("id: " + array[i].quote + "," + array[i].frequency);
        }

    }

    public void quickSelect(QuoteFrequency[] arr, int low,
                                  int high, int k)
    {
        // find the partition
        int partition = partition(arr, low, high);

        // if partition value is equal to the kth position, return
        if (partition == k)
            return;
        else if (partition > k)
            quickSelect(arr,partition + 1, high, k);
        else
            quickSelect(arr, low, partition-1, k);
    }

    public int partition (QuoteFrequency[] arr,
                          int low, int high)
    {
        int pivot = arr[high].frequency, pivotloc = low;
        for (int i = low; i <= high; i++)
        {
            // inserting elements of higher value
            // to the left of the pivot location
            if(arr[i].frequency > pivot)
            {
                QuoteFrequency temp = arr[i];
                arr[i] = arr[pivotloc];
                arr[pivotloc] = temp;
                pivotloc++;
            }
        }

        // swapping pivot to the final pivot location
        QuoteFrequency temp = arr[high];
        arr[high] = arr[pivotloc];
        arr[pivotloc] = temp;

        return pivotloc;
    }

}

class QuoteFrequency {
    public String quote;
    public int frequency;
}
