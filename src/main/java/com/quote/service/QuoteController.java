package com.quote.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class QuoteController {

    @Autowired
    private QuoteService quoteService;


    @GetMapping("/quote/get")
    public List<String> getQuote(@RequestParam(value = "name", defaultValue = "World") String name) {
        return quoteService.getFrequentTickers();
    }

    @PostMapping("/quote/put")
    public void putQuote(@RequestParam(value = "quote") Quote quote){
        quoteService.putQuote(quote);
    }

}
